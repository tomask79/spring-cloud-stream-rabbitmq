package com.example.spring.cloud.endpoint;

import com.example.spring.cloud.configuration.SinkRabbitAPI;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.messaging.MessageChannel;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by tomask79 on 30.03.17.
 */
@Service
public class ProcessingAMPQEndpoint {

    @StreamListener(SinkRabbitAPI.INPUT_CITIES)
    public void processCity(final String city) {
        System.out.println("Trying to process input city: "+city);
    }

    @StreamListener(SinkRabbitAPI.INPUT_PERSONS)
    public void processPersons(final String person) {
        System.out.println("Trying to process input person: "+person);
        throw new RuntimeException();
    }
}
