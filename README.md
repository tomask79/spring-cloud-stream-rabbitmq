# Spring Cloud Stream and Event driven Microservices with RabbitMQ #

In this repo let's show how to use [Spring Cloud Stream](https://cloud.spring.io/spring-cloud-stream/) for designing the [event driven microservices](http://microservices.io/patterns/data/event-driven-architecture.html). First, what's so good on Spring Cloud Stream in the first place? Because [Spring AMPQ](https://projects.spring.io/spring-amqp/) offers everything you need to access AMPQ artifacts. If you're not familiar with Spring AMPQ check this [repo](https://bitbucket.org/tomask79/spring-rabbitmq-examples/src) with many usefull examples. So why bother with Spring Cloud Stream...?

## Spring Cloud Stream concept ##

* Spring Cloud Stream binds used message broker with your Spring Integration message channels through [Binder](http://docs.spring.io/spring-cloud-stream/docs/Brooklyn.SR3/reference/htmlsingle/#_the_binder_abstraction) concept. RabbitMQ and Kafka are supported.

* Spring Cloud Stream separates infrastructure configuration out of the code into property files. This means that your [Spring Integration](https://projects.spring.io/spring-integration/) code will be the same even if you change underlying broker! 

## Spring Cloud Stream concept in example (RabbitMQ) ##

Lets have one **exchange called streamInput** with two queues **streamInput.cities** and **streamInput.persons**. Now lets plug these two queues to two message channels **citiesChannel** and **personsChannel** to consume incoming messages from that. With Spring AMPQ you'd need to create [SimpleMessageListenerContainer ](http://docs.spring.io/spring-amqp/api/org/springframework/amqp/rabbit/listener/SimpleMessageListenerContainer.html) and wire the infrastructure in the code. But this is a lot of boilerplate code. With Spring Cloud Stream you can separate AMPQ configuration to property file:

```
spring.cloud.stream.bindings.citiesChannel.destination=streamInput
spring.cloud.stream.bindings.citiesChannel.group=cities
spring.cloud.stream.rabbit.bindings.citiesChannel.consumer.durableSubscription=true
spring.cloud.stream.rabbit.bindings.citiesChannel.consumer.bindingRoutingKey=cities

spring.cloud.stream.bindings.personsChannel.destination=streamInput
spring.cloud.stream.bindings.personsChannel.group=persons
spring.cloud.stream.rabbit.bindings.personsChannel.consumer.durableSubscription=true
spring.cloud.stream.rabbit.bindings.personsChannel.consumer.bindingRoutingKey=persons
```

### Config in details ###

With RabbitMQ Binder on classpath every destination is mapped to the [TopicExchange ](http://docs.spring.io/autorepo/docs/spring-amqp-dist/1.5.6.RELEASE/api/org/springframework/amqp/core/TopicExchange.html). In the example I created **TopicExchange** called **streamInput** 
and attached it to two message channels **citiesChannel** and **personsChannel**.

```
spring.cloud.stream.bindings.citiesChannel.destination=streamInput
spring.cloud.stream.bindings.personsChannel.destination=streamInput
```

Now you need to understand that RabbitMQ binder is inspired by Kafka that consumers of the queues are grouped into consumer groups where only one consumer will get the message. 
This makes sense, because you can then easily scale your consumers up. So lets create two queues **streamInput.persons** and **streamInput.cities** and attach them to **streamInput** TopicExchange and mentioned message channels

```
# This will create queue "streamInput.cities" connected to message channel citiesChannel where input messages will land.
spring.cloud.stream.bindings.citiesChannel.group=cities 

# Durable subscription, of course.
spring.cloud.stream.rabbit.bindings.citiesChannel.consumer.durableSubscription=true 

# AMPQ binding to exchange (previous spring.cloud.stream.bindings.<channel name>.destination settings).
# Only messages with routingKey = 'cities' will land here.
spring.cloud.stream.rabbit.bindings.citiesChannel.consumer.bindingRoutingKey=cities 

spring.cloud.stream.bindings.personsChannel.group=persons
spring.cloud.stream.rabbit.bindings.personsChannel.consumer.durableSubscription=true
spring.cloud.stream.rabbit.bindings.personsChannel.consumer.bindingRoutingKey=persons
```

### Connecting properties to Spring Integration ###

Okay, I created two queues so far. **StreamInput.cities** binded to **citiesChannel**. **StreamInput.persons** binded to **personsChannel**. 
Yes, **<destination>.<group>** is the naming convention for queues which spring cloud stream follows. Now lets connect it to Spring Integration:

```
package com.example.spring.cloud.configuration;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.messaging.SubscribableChannel;

/**
 * Created by tomask79 on 30.03.17.
 */
public interface SinkRabbitAPI {

    String INPUT_CITIES = "citiesChannel";

    String INPUT_PERSONS = "personsChannel";

    @Input(SinkRabbitAPI.INPUT_CITIES)
    SubscribableChannel citiesChannel();

    @Input(SinkRabbitAPI.INPUT_PERSONS)
    SubscribableChannel personsChannel();
}
```
and load it on Spring Boot application on startup:

```
package com.example.spring.cloud;

import com.example.spring.cloud.configuration.SinkRabbitAPI;
import com.example.spring.cloud.configuration.SourceRabbitAPI;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableBinding({SinkRabbitAPI.class})
public class StreamingApplication {

	public static void main(String[] args) {
		SpringApplication.run(StreamingApplication.class, args);
	}
}
```

after this, we can create consumers receiving messages from queues in the wired message channels:

```
import com.example.spring.cloud.configuration.SinkRabbitAPI;
import com.example.spring.cloud.configuration.SourceRabbitAPI;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by tomask79 on 30.03.17.
 */
@Service
public class ProcessingAMPQEndpoint {

    @StreamListener(SinkRabbitAPI.INPUT_CITIES)
    public void processCity(final String city) {
        System.out.println("Trying to process input city: "+city);
    }

    @StreamListener(SinkRabbitAPI.INPUT_PERSONS)
    public void processPersons(final String person) {
        System.out.println("Trying to process input person: "+person);
    }
}
```
And that's it!
 
### RabbitMQ binder and broker configuration ###

Okay and how Spring Cloud Stream knows where to look for a broker? If RabbitMQ binder is found on the classpath
then default RabbitMQ host (localhost) and port (5672) is used to connect to RabbitMQ server. If your broker is at the different place
then look for properties:

```
spring:
  cloud:
    stream:
      bindings:
        ...
      binders:
          rabbitbinder:
            type: rabbit
            environment:
              spring:
                rabbitmq:
                  host: rabbitmq
                  port: 5672
                  username: XXX
                  password: XXX
```

## Testing the message consuming ##

* Install and run RabbitMQ broker
* git clone https://tomask79@bitbucket.org/tomask79/spring-cloud-stream-rabbitmq.git
* mvn clean install
* java -jar target/streaming-0.0.1-SNAPSHOT.jar
* Now publish message on streamInput Exchange with routing key 'cities' or 'persons'...output should be:

```
Started StreamingApplication in 6.513 seconds (JVM running for 6.92) 
Trying to process input city: sdjfjljksdflkjsdflkjsdfsfd
Trying to process input person: sdjfjljksdflkjsdflkjsdfsfd
```

## Message redelivery with Spring Cloud Stream ##

During message consuming a lot of things may go south and you typically want to try to receive the message again
before it will go to DLX exchange. First lets configure how many times Spring Cloud Stream is going to try redeliver
failed message:

```
spring.cloud.stream.bindings.personsChannel.consumer.maxAttempts=6

```
This means that if message receive from streamInput.persons queue will go wrong then Spring Cloud Stream will try
to redeliver it six times. Let's try that, first let's modify the receiving endpoint to simulate receive crash:

```
    @StreamListener(SinkRabbitAPI.INPUT_PERSONS)
    public void processPersons(final String person) {
        System.out.println("Trying to process input person: "+person);
        throw new RuntimeException();
    }
```

If I now try to publish something into streamInput exchange with persons routing key then this should be the output:

```
Trying to process input person: sfsdfsdfsd
Trying to process input person: sfsdfsdfsd
Trying to process input person: sfsdfsdfsd
Trying to process input person: sfsdfsdfsd
Trying to process input person: sfsdfsdfsd
Trying to process input person: sfsdfsdfsd
 Retry Policy Exhausted
        at org.springframework.amqp.rabbit.retry.RejectAndDontRequeueRecoverer.recover
(RejectAndDontRequeueRecoverer.java:45) ~[spring-rabbit-1.7.0.RELEASE.jar! /:na]
        at org.springframework.amqp.rabbit.config.StatelessRetryOperationsInterc       
```

that's it. In the company where I work I'm going to be suggesting to use Spring Cloud Stream
for event driven MicroServices, because it saves time and you don't need to write boilerplate code for
AMPQ infrastructure in Java.

regards

Tomas